# Домашнее задание
  1. Начало работы с LVM
  1. Уменьшить том под / до 8G
  2. Выделить том под /home
  3. Выделить том под /var (/var - сделать в mirror)
  4. Для /home - сделать том для снэпшотов
  5. Прописать монтирование в fstab (попробовать с разными опциями и разными файловыми системами на выбор)
  
  ##### Работа со снапшотами:
    1. Сгенерировать файлы в /home/
    2. Снять снэпшот
    3. Удалить часть файлов
    4. Восстановиться со снэпшота (залоггировать работу можно утилитой script, скриншотами и т.п.)
    5. На нашей куче дисков попробовать поставить btrfs/zfs:
       - с кешем и снэпшотами
       - разметить здесь каталог /opt

# Начало работы с LVM
  1. Тестовый стенд взял вот здесь
    `https://gitlab.com/otus_linux/stands-03-lvm`
  2. Запускаем тестовый стенд  
    `vagrant ssh`
  3. Размечаем диск LVM. Создаем PV
    
    >>>[root@lvm vagrant]# pvcreate /dev/sdb 
    >>>Physical volume "/dev/sdb" successfully created.
       
  4. Создаем первый уровень абстракции VG
    
    >>>[root@lvm vagrant]# vgcreate naga /dev/sdb
    >>>Volume group "naga" successfully created
  
  5. Создаем LV

    [root@lvm vagrant]# lvcreate -l+80%FREE -n test naga
    WARNING: ext4 signature detected on /dev/naga/test at offset 1080. Wipe it? [y/n]: y
    Wiping ext4 signature on /dev/naga/test.
    Logical volume "test" created.

  6. Проверяем созданный VG

    [root@lvm vagrant]# vgdisplay naga
    --- Volume group ---
    VG Name               naga
    System ID
    Format                lvm2
    Metadata Areas        1
    Metadata Sequence No  2
    VG Access             read/write
    VG Status             resizable
    MAX LV                0
    Cur LV                1
    Open LV               0
    Max PV                0
    Cur PV                1
    Act PV                1
    VG Size               <10.00 GiB
    PE Size               4.00 MiB
    Total PE              2559
    Alloc PE / Size       2047 / <8.00 GiB
    Free  PE / Size       512 / 2.00 GiB
    VG UUID               yep3nb-D619-zWQQ-V2dX-sVTv-QWvr-lKx82V  
    [root@lvm vagrant]# vgdisplay -v naga | grep 'PV Name'
    PV Name               /dev/sdb
    [root@lvm vagrant]# lvdisplay /dev/naga/test
    --- Logical volume ---
    LV Path                /dev/naga/test
    LV Name                test
    VG Name                naga
    LV UUID                eawglI-36eD-qVG7-eUNR-HJ6P-0ZgG-U1bTJp
    LV Write Access        read/write
    LV Creation host, time lvm, 2022-03-01 05:52:22 +0000
    LV Status              available
    # open                 0
    LV Size                <8.00 GiB
    Current LE             2047
    Segments               1
    Allocation             inherit
    Read ahead sectors     auto
    - currently set to     8192
    Block device           253:2

  7. Создадим на LV файловую систему и смонтируем его
  
    [root@lvm vagrant]# mkfs.ext4 /dev/naga/test
    mke2fs 1.42.9 (28-Dec-2013)
    Filesystem label=
    OS type: Linux
    Block size=4096 (log=2)
    Fragment size=4096 (log=2)
    Stride=0 blocks, Stripe width=0 blocks
    524288 inodes, 2096128 blocks
    104806 blocks (5.00%) reserved for the super user
    First data block=0
    Maximum filesystem blocks=2147483648
    64 block groups
    32768 blocks per group, 32768 fragments per group
    8192 inodes per group
    Superblock backups stored on blocks:
            32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632

    Allocating group tables: done
    Writing inode tables: done
    Creating journal (32768 blocks): done
    Writing superblocks and filesystem accounting information: done

    [root@lvm vagrant]# mkdir /data
    mkdir: cannot create directory ‘/data’: File exists
    [root@lvm vagrant]# mount /dev/naga/test /data/
    [root@lvm vagrant]# mount |grep /data
    /dev/mapper/naga-test on /data type ext4 (rw,relatime,seclabel,data=ordered)
  
  8. Расширение LVM 
    
  Создаем PV
    
    [root@lvm vagrant]# pvcreate /dev/sdc
    Physical volume "/dev/sdc" successfully created.

  Расширям VG путем добавления нового диска

    [root@lvm vagrant]# vgextend naga /dev/sdc
    Volume group "naga" successfully extended

  Убедимсā что новýй диск присутствует в новой VG и места стало больше

    [root@lvm vagrant]# vgdisplay -v naga | grep 'PV Name'
    PV Name               /dev/sdb
    PV Name               /dev/sdc
    [root@lvm vagrant]# vgs
    VG         #PV #LV #SN Attr   VSize   VFree
    VolGroup00   1   2   0 wz--n- <38.97g     0
    naga         2   1   0 wz--n-  11.99g <4.00g
  
  Сымитируем занятое место с помощью команды dd

    [root@lvm vagrant]# dd if=/dev/zero of=/data/test.log bs=1M count=8000 status=progress
    8232370176 bytes (8.2 GB) copied, 27.164613 s, 303 MB/s
    dd: error writing ‘/data/test.log’: No space left on device
    7880+0 records in
    7879+0 records out
    8262189056 bytes (8.3 GB) copied, 27.2664 s, 303 MB/s
  
  Проверям свободное место на диске

    [root@lvm vagrant]# df -Th /data/
    Filesystem            Type  Size  Used Avail Use% Mounted on
    /dev/mapper/naga-test ext4  7.8G  7.8G     0 100% /data
  
  Увеличиваем место за счет ранее созданого PV и проверям

    [root@lvm vagrant]# lvextend -l+80%FREE /dev/naga/test
    Size of logical volume naga/test changed from <8.00 GiB (2047 extents) to <11.20 GiB (2866    extents).
    Logical volume naga/test successfully resized.
    [root@lvm vagrant]# lvs /dev/naga/test
    LV   VG   Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
    test naga -wi-ao---- <11.20g

  Файловая система осталось без изменений
    
    [root@lvm vagrant]# df -Th /data
    Filesystem            Type  Size  Used Avail Use% Mounted on
    /dev/mapper/naga-test ext4  7.8G  7.8G     0 100% /data

  Произведем ресайз файлов для добовления пространства на захламленный диск
    
    [root@lvm vagrant]# resize2fs /dev/naga/test
    resize2fs 1.42.9 (28-Dec-2013)
    Filesystem at /dev/naga/test is mounted on /data; on-line resizing required
    old_desc_blocks = 1, new_desc_blocks = 2
    The filesystem on /dev/naga/test is now 2934784 blocks long.

    [root@lvm vagrant]# df -Th /data
    Filesystem            Type  Size  Used Avail Use% Mounted on
    /dev/mapper/naga-test ext4   11G  7.8G  2.7G  75% /data  

  9. Уменьшение существующей LV с помощью команды lvreduce
   
  Отмонтируем /data и проверим на ошибки

    [root@lvm vagrant]# umount /data/
    [root@lvm vagrant]# e2fsck -fy /dev/naga/test
    e2fsck 1.42.9 (28-Dec-2013)
    Pass 1: Checking inodes, blocks, and sizes
    Pass 2: Checking directory structure
    Pass 3: Checking directory connectivity
    Pass 4: Checking reference counts
    Pass 5: Checking group summary information
    /dev/naga/test: 12/737280 files (0.0% non-contiguous), 2106421/2934784 blocks
 
  Делаем ресайз диска

    [root@lvm vagrant]# resize2fs /dev/naga/test 10G
    resize2fs 1.42.9 (28-Dec-2013)
    Resizing the filesystem on /dev/naga/test to 2621440 (4k) blocks.
    The filesystem on /dev/naga/test is now 2621440 blocks long.   
  
    [root@lvm vagrant]# lvreduce /dev/naga/test -L 10G
    WARNING: Reducing active logical volume to 10.00 GiB.
    THIS MAY DESTROY YOUR DATA (filesystem etc.)
    Do you really want to reduce naga/test? [y/n]: y
    Size of logical volume naga/test changed from <11.20 GiB (2866 extents) to 10.00 GiB (2560 extents).
    Logical volume naga/test successfully resized. 
    [root@lvm vagrant]# mount /dev/naga/test /data/

  Проверим размер диска
     
    [root@lvm vagrant]# df -Th /data/
    Filesystem            Type  Size  Used Avail Use% Mounted on
    /dev/mapper/naga-test ext4  9.8G  7.8G  1.6G  84% /data

    [root@lvm vagrant]# lvs /dev/naga/test
    LV   VG   Attr       LSize  Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
    test naga -wi-ao---- 10.00g

  10. LVM Snapshot
 
  Создадим снапшот с аомощью команды lvcreate, с флагом -s, который говорит о том что это снимок 
 
    [root@lvm vagrant]# lvcreate -L 500M -s -n test-snap /dev/naga/test
    Logical volume "test-snap" created.

  Проверяем
     
    [root@lvm vagrant]# vgs -o +lv_size,lv_name | grep test
    naga         2   2   1 wz--n-  11.99g 1.50g  10.00g test
    naga         2   2   1 wz--n-  11.99g 1.50g 500.00m test-snap

    [root@lvm vagrant]# lsblk
    NAME                    MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
    sda                       8:0    0   40G  0 disk
    ├─sda1                    8:1    0    1M  0 part
    ├─sda2                    8:2    0    1G  0 part /boot
    └─sda3                    8:3    0   39G  0 part
      ├─VolGroup00-LogVol00 253:0    0 37.5G  0 lvm  /
      └─VolGroup00-LogVol01 253:1    0  1.5G  0 lvm  [SWAP]
    sdb                       8:16   0   10G  0 disk
    └─naga-test-real        253:3    0   10G  0 lvm
      ├─naga-test           253:2    0   10G  0 lvm  /data
      └─naga-test--snap     253:5    0   10G  0 lvm
    sdc                       8:32   0    2G  0 disk
    ├─naga-test-real        253:3    0   10G  0 lvm
    │ ├─naga-test           253:2    0   10G  0 lvm  /data
    │ └─naga-test--snap     253:5    0   10G  0 lvm
    └─naga-test--snap-cow   253:4    0  500M  0 lvm
      └─naga-test--snap     253:5    0   10G  0 lvm
    sdd                       8:48   0    1G  0 disk
    sde                       8:64   0    1G  0 disk 

  Монтируем snapshot

    [root@lvm vagrant]# mkdir /data-snap
    mkdir: cannot create directory ‘/data-snap’: File exists
    [root@lvm vagrant]# mount /dev/naga/test-snap /data-snap/
    [root@lvm vagrant]# ll /data-snap/
    total 8068564
    drwx------. 2 root root      16384 Mar  1 06:07 lost+found
    -rw-r--r--. 1 root root 8262189056 Mar  1 06:20 test.log
    [root@lvm vagrant]# umount /data-snap

# Уменьшить том под / до 8G

  1. Установка утилиты xfsdump
  2. Этап подготовки тома для раздела
     
    [root@lvm vagrant]# pvcreate /dev/sdb
    Physical volume "/dev/sdb" successfully created.
    [root@lvm vagrant]# vgcreate vg_root /dev/sdb
    Volume group "vg_root" successfully created
    [root@lvm vagrant]# lvcreate -n lv_root -l +100%FREE /dev/vg_root
    WARNING: ext4 signature detected on /dev/vg_root/lv_root at offset 1080. Wipe it? [y/n]: y
    Wiping ext4 signature on /dev/vg_root/lv_root.
    Logical volume "lv_root" created.
    
  3. Монтируем раздел

    [root@lvm vagrant]# mount /dev/vg_root/lv_root /mnt
  
  4. Копируем данные в раздел /mnt
    
    [root@lvm vagrant]# xfsdump -J - /dev/VolGroup00/LogVol00 | xfsrestore -J - /mnt
    xfsdump: using file dump (drive_simple) strategy
    xfsdump: version 3.1.7 (dump format 3.0)
    xfsdump: level 0 dump of lvm:/
    xfsdump: dump date: Mon Mar  7 06:59:44 2022
    xfsdump: session id: d061780a-4995-495f-8d2e-97b588058e71
    xfsdump: session label: ""
    xfsrestore: using file dump (drive_simple) strategy
    xfsrestore: version 3.1.7 (dump format 3.0)
    xfsdump: ino map phase 1: constructing initial dump list
    xfsrestore: searching media for dump
    xfsdump: ino map phase 2: skipping (no pruning necessary)
    xfsdump: ino map phase 3: skipping (only one dump stream)
    xfsdump: ino map construction complete
    xfsdump: estimated dump size: 9779176896 bytes
    xfsdump: creating dump session media file 0 (media 0, file 0)
    xfsdump: dumping ino map
    xfsdump: dumping directories
    xfsrestore: examining media file 0
    xfsrestore: dump description:
    xfsrestore: hostname: lvm
    xfsrestore: mount point: /
    xfsrestore: volume: /dev/mapper/VolGroup00-LogVol00
    xfsrestore: session time: Mon Mar  7 06:59:44 2022
    xfsrestore: level: 0
    xfsrestore: session label: ""
    xfsrestore: media label: ""
    xfsrestore: file system id: b60e9498-0baa-4d9f-90aa-069048217fee
    xfsrestore: session id: d061780a-4995-495f-8d2e-97b588058e71
    xfsrestore: media id: aa537f6a-b2aa-4831-b683-f9a9ccdbab30
    xfsrestore: searching media for directory dump
    xfsrestore: reading directories
    xfsdump: dumping non-directory files
    xfsrestore: 4046 directories and 33932 entries processed
    xfsrestore: directory post-processing
    xfsrestore: restoring non-directory files
    xfsdump: ending media file
    xfsdump: media file size 9747347696 bytes
    xfsdump: dump size (non-dir files) : 9727615896 bytes
    xfsdump: dump complete: 257 seconds elapsed
    xfsdump: Dump Status: SUCCESS
    xfsrestore: restore complete: 257 seconds elapsed
    xfsrestore: Restore Status: SUCCESS

  5. Переконфигурируем grub для того, чтобы при старте перейти в новую /
    
    [root@lvm vagrant]# for i in /proc/ /sys/ /dev/ /run/ /boot/; do mount --bind $i /mnt/$i; done
    [root@lvm vagrant]# chroot /mnt/
    [root@lvm /]# grub2-mkconfig -o /boot/grub2/grub.cfg
    Generating grub configuration file ...
    Found linux image: /boot/vmlinuz-3.10.0-1160.53.1.el7.x86_64
    Found initrd image: /boot/initramfs-3.10.0-1160.53.1.el7.x86_64.img
    Found linux image: /boot/vmlinuz-3.10.0-862.2.3.el7.x86_64
    Found initrd image: /boot/initramfs-3.10.0-862.2.3.el7.x86_64.img
    done

  6. Обновим образ initrd.

    [root@lvm /]# cd /boot ; for i in `ls initramfs-*img`; do dracut -v $i `echo $i|sed "s/initramfs-//g;
    > s/.img//g"` --force; done
    Executing: /sbin/dracut -v initramfs-3.10.0-1160.53.1.el7.x86_64.img 3.10.0-1160.53.1.el7.x86_64 --force
    dracut module 'busybox' will not be installed, because command 'busybox' could not be found!
    dracut module 'crypt' will not be installed, because command 'cryptsetup' could not be found!
    dracut module 'dmraid' will not be installed, because command 'dmraid' could not be found!
    dracut module 'dmsquash-live-ntfs' will not be installed, because command 'ntfs-3g' could not be found!
    dracut module 'multipath' will not be installed, because command 'multipath' could not be found!
    dracut module 'busybox' will not be installed, because command 'busybox' could not be found!
    dracut module 'crypt' will not be installed, because command 'cryptsetup' could not be found!
    dracut module 'dmraid' will not be installed, because command 'dmraid' could not be found!
    dracut module 'dmsquash-live-ntfs' will not be installed, because command 'ntfs-3g' could not be found!
    dracut module 'multipath' will not be installed, because command 'multipath' could not be found!
    *** Including module: bash ***
    *** Including module: nss-softokn ***
    *** Including module: i18n ***
    *** Including module: drm ***
    *** Including module: plymouth ***
    *** Including module: dm ***
    Skipping udev rule: 64-device-mapper.rules
    Skipping udev rule: 60-persistent-storage-dm.rules
    Skipping udev rule: 55-dm.rules
    *** Including module: kernel-modules ***
    Omitting driver floppy
    *** Including module: lvm ***
    Skipping udev rule: 64-device-mapper.rules
    Skipping udev rule: 56-lvm.rules
    Skipping udev rule: 60-persistent-storage-lvm.rules
    *** Including module: qemu ***
    *** Including module: resume ***
    *** Including module: rootfs-block ***
    *** Including module: terminfo ***
    *** Including module: udev-rules ***
    Skipping udev rule: 40-redhat-cpu-hotplug.rules
    Skipping udev rule: 91-permissions.rules
    *** Including module: biosdevname ***
    *** Including module: systemd ***
    *** Including module: usrmount ***
    *** Including module: base ***
    *** Including module: fs-lib ***
    *** Including module: shutdown ***
    *** Including modules done ***
    *** Installing kernel module dependencies and firmware ***
    *** Installing kernel module dependencies and firmware done ***
    *** Resolving executable dependencies ***
    *** Resolving executable dependencies done***
    *** Hardlinking files ***
    *** Hardlinking files done ***
    *** Stripping files ***
    *** Stripping files done ***
    *** Generating early-microcode cpio image contents ***
    *** No early-microcode cpio image needed ***
    *** Store current command line parameters ***
    *** Creating image file ***
    *** Creating image file done ***
    *** Creating initramfs image file '/boot/initramfs-3.10.0-1160.53.1.el7.x86_64.img' done ***
    Executing: /sbin/dracut -v initramfs-3.10.0-862.2.3.el7.x86_64.img 3.10.0-862.2.3.el7.x86_64 --force
    dracut module 'busybox' will not be installed, because command 'busybox' could not be found!
    dracut module 'crypt' will not be installed, because command 'cryptsetup' could not be found!
    dracut module 'dmraid' will not be installed, because command 'dmraid' could not be found!
    dracut module 'dmsquash-live-ntfs' will not be installed, because command 'ntfs-3g' could not be found!
    dracut module 'multipath' will not be installed, because command 'multipath' could not be found!
    dracut module 'busybox' will not be installed, because command 'busybox' could not be found!
    dracut module 'crypt' will not be installed, because command 'cryptsetup' could not be found!
    dracut module 'dmraid' will not be installed, because command 'dmraid' could not be found!
    dracut module 'dmsquash-live-ntfs' will not be installed, because command 'ntfs-3g' could not be found!
    dracut module 'multipath' will not be installed, because command 'multipath' could not be found!
    *** Including module: bash ***
    *** Including module: nss-softokn ***
    *** Including module: i18n ***
    *** Including module: drm ***
    *** Including module: plymouth ***
    *** Including module: dm ***
    Skipping udev rule: 64-device-mapper.rules
    Skipping udev rule: 60-persistent-storage-dm.rules
    Skipping udev rule: 55-dm.rules
    *** Including module: kernel-modules ***
    Omitting driver floppy
    *** Including module: lvm ***
    Skipping udev rule: 64-device-mapper.rules
    Skipping udev rule: 56-lvm.rules
    Skipping udev rule: 60-persistent-storage-lvm.rules
    *** Including module: qemu ***
    *** Including module: resume ***
    *** Including module: rootfs-block ***
    *** Including module: terminfo ***
    *** Including module: udev-rules ***
    Skipping udev rule: 40-redhat-cpu-hotplug.rules
    Skipping udev rule: 91-permissions.rules
    *** Including module: biosdevname ***
    *** Including module: systemd ***
    *** Including module: usrmount ***
    *** Including module: base ***
    *** Including module: fs-lib ***
    *** Including module: shutdown ***
    *** Including modules done ***
    *** Installing kernel module dependencies and firmware ***
    *** Installing kernel module dependencies and firmware done ***
    *** Resolving executable dependencies ***
    *** Resolving executable dependencies done***
    *** Hardlinking files ***
    *** Hardlinking files done ***
    *** Stripping files ***
    *** Stripping files done ***
    *** Generating early-microcode cpio image contents ***
    *** No early-microcode cpio image needed ***
    *** Store current command line parameters ***
    *** Creating image file ***
    *** Creating image file done ***
    *** Creating initramfs image file '/boot/initramfs-3.10.0-862.2.3.el7.x86_64.img' done ***
  
  7. Перезагружаемсā успешно с новýм рут томом. Убедитþсā в ÿтом можно посмотрев вýвод 
lsblk:
    
    [root@lvm vagrant]# lsblk
    NAME                    MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
    sda                       8:0    0   40G  0 disk
    ├─sda1                    8:1    0    1M  0 part
    ├─sda2                    8:2    0    1G  0 part /mnt/boot
    └─sda3                    8:3    0   39G  0 part
      ├─VolGroup00-LogVol01 253:1    0  1.5G  0 lvm  [SWAP]
      └─VolGroup00-LogVol00 253:2    0 37.5G  0 lvm
    sdb                       8:16   0   10G  0 disk
    └─vg_root-lv_root       253:0    0   10G  0 lvm  /mnt
    sdc                       8:32   0    2G  0 disk
    sdd                       8:48   0    1G  0 disk
    sde                       8:64   0    1G  0 disk

  8. Изменяем размер старой VG и возвращаем на него рут, для этого удаляем старую LV размеров в 40G и создаем новую на 8G:

    [root@lvm vagrant]# lvremove /dev/VolGroup00/LogVol00
    Do you really want to remove active logical volume VolGroup00/LogVol00? [y/n]: н
    WARNING: Invalid input ''.
    Do you really want to remove active logical volume VolGroup00/LogVol00? [y/n]: y
    Logical volume "LogVol00" successfully removed
    [root@lvm vagrant]# lvcreate -n VolGroup00/LogVol00 -L 8G /dev/VolGroup00
    WARNING: xfs signature detected on /dev/VolGroup00/LogVol00 at offset 0. Wipe it? [y/n]: y
    Wiping xfs signature on /dev/VolGroup00/LogVol00.
    Logical volume "LogVol00" created.
    [root@lvm vagrant]# mkfs.xfs /dev/VolGroup00/LogVol00
    meta-data=/dev/VolGroup00/LogVol00 isize=512    agcount=4, agsize=524288 blks
            =                       sectsz=512   attr=2, projid32bit=1
            =                       crc=1        finobt=0, sparse=0
    data    =                       bsize=4096   blocks=2097152, imaxpct=25
            =                       sunit=0      swidth=0 blks
    naming  =version 2              bsize=4096   ascii-ci=0 ftype=1
    log     =internal log           bsize=4096   blocks=2560, version=2
            =                       sectsz=512   sunit=0 blks, lazy-count=1
    realtime =none                  extsz=4096   blocks=0, rtextents=0
    [root@lvm vagrant]# mount /dev/VolGroup00/LogVol00 /mnt
    [root@lvm vagrant]# xfsdump -J - /dev/vg_root/lv_root | xfsrestore -J - /mnt
    xfsrestore: using file dump (drive_simple) strategy
    xfsrestore: version 3.1.7 (dump format 3.0)
    xfsdump: using file dump (drive_simple) strategy
    xfsdump: version 3.1.7 (dump format 3.0)
    xfsdump: level 0 dump of lvm:/mnt
    xfsdump: dump date: Mon Mar  7 09:15:15 2022
    xfsdump: session id: daa79a2a-3039-4311-a853-a17868a56871
    xfsdump: session label: ""
    xfsrestore: searching media for dump
    xfsdump: ino map phase 1: constructing initial dump list
    xfsdump: ino map phase 2: skipping (no pruning necessary)
    xfsdump: ino map phase 3: skipping (only one dump stream)
    xfsdump: ino map construction complete
    xfsdump: estimated dump size: 41920 bytes
    xfsdump: creating dump session media file 0 (media 0, file 0)
    xfsdump: dumping ino map
    xfsdump: dumping directories
    xfsdump: dumping non-directory files
    xfsdump: ending media file
    xfsdump: media file size 42840 bytes
    xfsdump: dump size (non-dir files) : 20512 bytes
    xfsdump: dump complete: 0 seconds elapsed
    xfsdump: Dump Status: SUCCESS
    xfsrestore: examining media file 0
    xfsrestore: dump description:
    xfsrestore: hostname: lvm
    xfsrestore: mount point: /mnt
    xfsrestore: volume: /dev/mapper/vg_root-lv_root
    xfsrestore: session time: Mon Mar  7 09:15:15 2022
    xfsrestore: level: 0
    xfsrestore: session label: ""
    xfsrestore: media label: ""
    xfsrestore: file system id: 2fd5caab-dc82-47f9-b581-5ad79b8861fc
    xfsrestore: session id: daa79a2a-3039-4311-a853-a17868a56871
    xfsrestore: media id: e32cc86d-bc09-4f10-ac31-9b251449dee7
    xfsrestore: searching media for directory dump
    xfsrestore: reading directories
    xfsrestore: 2 directories and 2 entries processed
    xfsrestore: directory post-processing
    xfsrestore: restoring non-directory files
    xfsrestore: restore complete: 0 seconds elapsed
    xfsrestore: Restore Status: SUCCESS

  #### Так же как в первый раз переконфигурируем grub, за исключением правки /etc/grub2/grub.cfg
  
    [root@lvm vagrant]# mount /dev/VolGroup00/LogVol00 /mnt
    mount: /dev/mapper/VolGroup00-LogVol00 is already mounted or /mnt busy
           /dev/mapper/VolGroup00-LogVol00 is already mounted on /mnt
    [root@lvm vagrant]# lsblk
    NAME                    MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
    sda                       8:0    0   40G  0 disk
    ├─sda1                    8:1    0    1M  0 part
    ├─sda2                    8:2    0    1G  0 part /mnt/boot
    └─sda3                    8:3    0   39G  0 part
      ├─VolGroup00-LogVol01 253:1    0  1.5G  0 lvm  [SWAP]
      └─VolGroup00-LogVol00 253:2    0    8G  0 lvm  /mnt
    sdb                       8:16   0   10G  0 disk
    └─vg_root-lv_root       253:0    0   10G  0 lvm  /mnt
    sdc                       8:32   0    2G  0 disk
    sdd                       8:48   0    1G  0 disk
    sde                       8:64   0    1G  0 disk
    [root@lvm vagrant]# for i in /proc/ /sys/ /dev/ /run/ /boot/; do mount --bind $i /mnt/$i; done
    mount: mount point /mnt//proc/ does not exist
    mount: mount point /mnt//sys/ does not exist
    mount: mount point /mnt//dev/ does not exist
    mount: mount point /mnt//run/ does not exist
    mount: mount point /mnt//boot/ does not exist
    [root@lvm vagrant]# umount /dev/vg_root/lv_root /mnt
    umount: /dev/vg_root/lv_root: umount failed: Invalid argument
    [root@lvm vagrant]# chroot /mnt/
    [root@lvm /]# grub2-mkconfig -o /boot/grub2/grub.cfg
    Generating grub configuration file ...
    Found linux image: /boot/vmlinuz-3.10.0-1160.53.1.el7.x86_64
    Found initrd image: /boot/initramfs-3.10.0-1160.53.1.el7.x86_64.img
    Found linux image: /boot/vmlinuz-3.10.0-862.2.3.el7.x86_64
    Found initrd image: /boot/initramfs-3.10.0-862.2.3.el7.x86_64.img
    done
    [root@lvm /]# cd /boot ; for i in `ls initramfs-*img`; do dracut -v $i `echo $i|sed "s/initramfs-//g;
    > s/.img//g"` --force; done
    Executing: /sbin/dracut -v initramfs-3.10.0-1160.53.1.el7.x86_64.img 3.10.0-1160.53.1.el7.x86_64 --force
    dracut module 'busybox' will not be installed, because command 'busybox' could not be found!
    dracut module 'crypt' will not be installed, because command 'cryptsetup' could not be found!
    dracut module 'dmraid' will not be installed, because command 'dmraid' could not be found!
    dracut module 'dmsquash-live-ntfs' will not be installed, because command 'ntfs-3g' could not be found!
    dracut module 'multipath' will not be installed, because command 'multipath' could not be found!
    dracut module 'busybox' will not be installed, because command 'busybox' could not be found!
    dracut module 'crypt' will not be installed, because command 'cryptsetup' could not be found!
    dracut module 'dmraid' will not be installed, because command 'dmraid' could not be found!
    dracut module 'dmsquash-live-ntfs' will not be installed, because command 'ntfs-3g' could not be found!
    dracut module 'multipath' will not be installed, because command 'multipath' could not be found!
    *** Including module: bash ***
    *** Including module: nss-softokn ***
    *** Including module: i18n ***
    *** Including module: drm ***
    *** Including module: plymouth ***
    *** Including module: dm ***
    Skipping udev rule: 64-device-mapper.rules
    Skipping udev rule: 60-persistent-storage-dm.rules
    Skipping udev rule: 55-dm.rules
    *** Including module: kernel-modules ***
    Omitting driver floppy
    *** Including module: lvm ***
    Skipping udev rule: 64-device-mapper.rules
    Skipping udev rule: 56-lvm.rules
    Skipping udev rule: 60-persistent-storage-lvm.rules
    *** Including module: qemu ***
    *** Including module: resume ***
    *** Including module: rootfs-block ***
    *** Including module: terminfo ***
    *** Including module: udev-rules ***
    Skipping udev rule: 40-redhat-cpu-hotplug.rules
    Skipping udev rule: 91-permissions.rules
    *** Including module: biosdevname ***
    *** Including module: systemd ***
    *** Including module: usrmount ***
    *** Including module: base ***
    *** Including module: fs-lib ***
    *** Including module: shutdown ***
    *** Including modules done ***
    *** Installing kernel module dependencies and firmware ***
    *** Installing kernel module dependencies and firmware done ***
    *** Resolving executable dependencies ***
    *** Resolving executable dependencies done***
    *** Hardlinking files ***
    *** Hardlinking files done ***
    *** Stripping files ***
    *** Stripping files done ***
    *** Generating early-microcode cpio image contents ***
    *** No early-microcode cpio image needed ***
    *** Store current command line parameters ***
    *** Creating image file ***
    *** Creating image file done ***
    *** Creating initramfs image file '/boot/initramfs-3.10.0-1160.53.1.el7.x86_64.img' done ***
    Executing: /sbin/dracut -v initramfs-3.10.0-862.2.3.el7.x86_64.img 3.10.0-862.2.3.el7.x86_64 --force
    dracut module 'busybox' will not be installed, because command 'busybox' could not be found!
    dracut module 'crypt' will not be installed, because command 'cryptsetup' could not be found!
    dracut module 'dmraid' will not be installed, because command 'dmraid' could not be found!
    dracut module 'dmsquash-live-ntfs' will not be installed, because command 'ntfs-3g' could not be found!
    dracut module 'multipath' will not be installed, because command 'multipath' could not be found!
    dracut module 'busybox' will not be installed, because command 'busybox' could not be found!
    dracut module 'crypt' will not be installed, because command 'cryptsetup' could not be found!
    dracut module 'dmraid' will not be installed, because command 'dmraid' could not be found!
    dracut module 'dmsquash-live-ntfs' will not be installed, because command 'ntfs-3g' could not be found!
    dracut module 'multipath' will not be installed, because command 'multipath' could not be found!
    *** Including module: bash ***
    *** Including module: nss-softokn ***
    *** Including module: i18n ***
    *** Including module: drm ***
    *** Including module: plymouth ***
    *** Including module: dm ***
    Skipping udev rule: 64-device-mapper.rules
    Skipping udev rule: 60-persistent-storage-dm.rules
    Skipping udev rule: 55-dm.rules
    *** Including module: kernel-modules ***
    Omitting driver floppy
    *** Including module: lvm ***
    Skipping udev rule: 64-device-mapper.rules
    Skipping udev rule: 56-lvm.rules
    Skipping udev rule: 60-persistent-storage-lvm.rules
    *** Including module: qemu ***
    *** Including module: resume ***
    *** Including module: rootfs-block ***
    *** Including module: terminfo ***
    *** Including module: udev-rules ***
    Skipping udev rule: 40-redhat-cpu-hotplug.rules
    Skipping udev rule: 91-permissions.rules
    *** Including module: biosdevname ***
    *** Including module: systemd ***
    *** Including module: usrmount ***
    *** Including module: base ***
    *** Including module: fs-lib ***
    *** Including module: shutdown ***
    *** Including modules done ***
    *** Installing kernel module dependencies and firmware ***
    *** Installing kernel module dependencies and firmware done ***
    *** Resolving executable dependencies ***
    *** Resolving executable dependencies done***
    *** Hardlinking files ***
    *** Hardlinking files done ***
    *** Stripping files ***
    *** Stripping files done ***
    *** Generating early-microcode cpio image contents ***
    *** No early-microcode cpio image needed ***
    *** Store current command line parameters ***
    *** Creating image file ***
    *** Creating image file done ***
    *** Creating initramfs image file '/boot/initramfs-3.10.0-862.2.3.el7.x86_64.img' done ***

  #### Пока не перезагружаемся и не выходим из под chroot - мы можем заодно перенести /var
 
    >На свободнýх дисках создаем зеркало:
    
    [root@lvm boot]# pvcreate /dev/sdc /dev/sdd
    Physical volume "/dev/sdc" successfully created.
    Physical volume "/dev/sdd" successfully created.
    [root@lvm boot]# vgcreate vg_var /dev/sdc /dev/sdd
    Volume group "vg_var" successfully created
    [root@lvm boot]# lvcreate -L
    lvcreate: option requires an argument -- 'L'
    Error during parsing of command line.
    [root@lvm boot]# lvcreate -L 950M -m1 -n lv_var vg_var
    Rounding up size to full physical extent 952.00 MiB
    Logical volume "lv_var" created.

    >Создаем на нем ФС и перемещаем туда /var:

    [root@lvm boot]# mkfs.ext4 /dev/vg_var/lv_var
    mke2fs 1.42.9 (28-Dec-2013)
    Filesystem label=
    OS type: Linux
    Block size=4096 (log=2)
    Fragment size=4096 (log=2)
    Stride=0 blocks, Stripe width=0 blocks
    60928 inodes, 243712 blocks
    12185 blocks (5.00%) reserved for the super user
    First data block=0
    Maximum filesystem blocks=249561088
    8 block groups
    32768 blocks per group, 32768 fragments per group
    7616 inodes per group
    Superblock backups stored on blocks:
            32768, 98304, 163840, 229376

    Allocating group tables: done
    Writing inode tables: done
    Creating journal (4096 blocks): done
    Writing superblocks and filesystem accounting information: done

    [root@lvm boot]# mount /dev/vg_var/lv_var /mnt
    [root@lvm boot]# cp -aR /var/* /mnt/ # rsync -avHPSAX /var/ /mnt/

    >На всякий случай сохраняем содержимое старого var (или же можно его просто удалить), монтируем var в var каталог, правим fstab:

    [root@lvm boot]# mkdir /tmp/oldvar && mv /var/* /tmp/oldvar
    mkdir: cannot create directory ‘/tmp/oldvar’: File exists
    [root@lvm boot]# umount /mnt
    [root@lvm boot]# mount /dev/vg_var/lv_var /var
    [root@lvm boot]# echo "`blkid | grep var: | awk '{print $2}'` /var ext4 defaults 0 0" >> /etc/fstab
    
    >Идем на перезагрузку и смотрим команду lsblk

    adminroot@wvds136654:~/git/stands-03-lvm$ vagrant ssh
    Last login: Wed Mar  9 05:44:41 2022 from 10.0.2.2
    [vagrant@lvm ~]$ lsblk
    NAME                     MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
    sda                        8:0    0   40G  0 disk
    ├─sda1                     8:1    0    1M  0 part
    ├─sda2                     8:2    0    1G  0 part /boot
    └─sda3                     8:3    0   39G  0 part
      ├─VolGroup00-LogVol00  253:0    0    8G  0 lvm  /
      └─VolGroup00-LogVol01  253:1    0  1.5G  0 lvm  [SWAP]
    sdb                        8:16   0   10G  0 disk
    └─vg_root-lv_root        253:2    0   10G  0 lvm
    sdc                        8:32   0    2G  0 disk
    ├─vg_var-lv_var_rmeta_0  253:3    0    4M  0 lvm
    │ └─vg_var-lv_var        253:7    0  952M  0 lvm  /var
    └─vg_var-lv_var_rimage_0 253:4    0  952M  0 lvm
      └─vg_var-lv_var        253:7    0  952M  0 lvm  /var
    sdd                        8:48   0    1G  0 disk
    ├─vg_var-lv_var_rmeta_1  253:5    0    4M  0 lvm
    │ └─vg_var-lv_var        253:7    0  952M  0 lvm  /var
    └─vg_var-lv_var_rimage_1 253:6    0  952M  0 lvm
      └─vg_var-lv_var        253:7    0  952M  0 lvm  /var
    sde                        8:64   0    1G  0 disk

    >Удалеяем верменную Volume Group

    [vagrant@lvm ~]$ sudo su
    [root@lvm vagrant]# lvremove /dev/vg_root/lv_root
    Do you really want to remove active logical volume vg_root/lv_root? [y/n]: y
    Logical volume "lv_root" successfully removed
    [root@lvm vagrant]# vgremove /dev/vg_root
    Volume group "vg_root" successfully removed
    [root@lvm vagrant]# pvremove /dev/sdb
    Labels on physical volume "/dev/sdb" successfully wiped.

    >Выделяем том под /home по тому же принципу что делали для /var:

    [root@lvm vagrant]# lvcreate -n LogVol_Home -L 2G /dev/VolGroup00
    Logical volume "LogVol_Home" created.
    [root@lvm vagrant]# mkfs.xfs /dev/VolGroup00/LogVol_Home
    meta-data=/dev/VolGroup00/LogVol_Home isize=512    agcount=4, agsize=131072 blks
             =                       sectsz=512   attr=2, projid32bit=1
             =                       crc=1        finobt=0, sparse=0
    data     =                       bsize=4096   blocks=524288, imaxpct=25
             =                       sunit=0      swidth=0 blks
    naming   =version 2              bsize=4096   ascii-ci=0 ftype=1
    log      =internal log           bsize=4096   blocks=2560, version=2
             =                       sectsz=512   sunit=0 blks, lazy-count=1
    realtime =none                   extsz=4096   blocks=0, rtextents=0
    [root@lvm vagrant]# mount /dev/VolGroup00/LogVol_Home /mnt/
    [root@lvm vagrant]# cp -aR /home/* /mnt/
    [root@lvm vagrant]# rm -rf /home/*
    [root@lvm vagrant]# umount /mnt
    [root@lvm vagrant]# mount /dev/VolGroup00/LogVol_Home /home/
    [root@lvm vagrant]# echo "`blkid | grep Home | awk '{print $2}'` /home xfs defaults 0 0" >> /etc/fstab

    >Создадим файлы в каталоге /home

    [root@lvm vagrant]# touch /home/file{1..20}
    [root@lvm vagrant]# ls /home
    file1   file11  file13  file15  file17  file19  file20  file4  file6  file8  vagrant
    file10  file12  file14  file16  file18  file2   file3   file5  file7  file9

    >Снимем снапшот

    [root@lvm vagrant]# lvcreate -L100MB -s -n home_snap /dev/VolGroup00/LogVol_Home
    Rounding up size to full physical extent 128.00 MiB
    Logical volume "home_snap" created.

    >Удалим часть файлов

    [root@lvm vagrant]# rm -f /home/file{11..20}
    [root@lvm vagrant]# ls /home
    file1  file10  file2  file3  file4  file5  file6  file7  file8  file9  vagrant

    >Откат со снапшота

    [root@lvm vagrant]# umount /home
    [root@lvm vagrant]# lvconvert --merge /dev/VolGroup00/home_snap
    Merging of volume VolGroup00/home_snap started.
    VolGroup00/LogVol_Home: Merged: 100.00%
    [root@lvm vagrant]# mount /home
    [root@lvm vagrant]# ls /home
    file1   file11  file13  file15  file17  file19  file20  file4  file6  file8  vagrant
    file10  file12  file14  file16  file18  file2   file3   file5  file7  file9
